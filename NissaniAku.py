import pandas as pd
import matplotlib.pyplot as plt
from numpy import arange


def mootmistulemused(tryki_ekraanile=False):
    andmed = pd.DataFrame(
        columns=('t', 'U'),
        data=[
    # ------------ MÕÕTMISTULEMUSTE TABEL -----------------
    #                             aeg         pinge / V
    # -----------------------------------------------------
            [pd.Timestamp('2024-02-04 11:45'), 13.20],  # enne ühendamist
            [pd.Timestamp('2024-02-04 12:00'), 13.01],
            [pd.Timestamp('2024-02-04 13:15'), 13.23],  # pärast sõitmist
            [pd.Timestamp('2024-02-04 17:25'), 12.93],
            [pd.Timestamp('2024-02-04 18:00'), 13.32],  # pärast sõitmist
            [pd.Timestamp('2024-02-05 11:29'), 12.82],
            [pd.Timestamp('2024-02-05 16:20'), 12.79],
            [pd.Timestamp('2024-02-07 15:32'), 12.63],
            [pd.Timestamp('2024-02-09 11:50'), 12.57],
            [pd.Timestamp('2024-02-10 15:33'), 12.55],
            [pd.Timestamp('2024-02-12 16:53'), 12.57],
            [pd.Timestamp('2024-02-14 10:57'), 12.54],
            [pd.Timestamp('2024-02-16 16:55'), 12.41],
            [pd.Timestamp('2024-02-18 11:42'), 12.36],
            [pd.Timestamp('2024-02-19 16:22'), 12.36],
            [pd.Timestamp('2024-02-21 15:36'), 12.30],
            [pd.Timestamp('2024-02-23 12:04'), 12.24],
            [pd.Timestamp('2024-02-26 16:25'), 12.24],
            [pd.Timestamp('2024-03-01 11:42'), 12.20],
            [pd.Timestamp('2024-03-04 11:58'), 12.07],
            [pd.Timestamp('2024-03-08 16:52'), 12.16],
            [pd.Timestamp('2024-03-10 11:44'), 12.08],
    # -----------------------------------------------------
        ]
       )
    
    std_err_func = lambda rdg: 0.008*rdg + 0.03
    std_err = std_err_func(andmed['U'])
    andmed.insert(2, 'dU', std_err)
    
    if tryki_ekraanile: print(andmed)
    
    df1 = andmed.loc[[0, 2, 4], :]
    df2 = andmed.drop([0, 2, 4])

    return df1, df2


def graafik1(df1, df2):
    label1 = 'enne ühendamist/pärast sõitmist'
    label2 = 'ühendatud, enne sõitmist'
    plt.figure(figsize=(10.5, 6))
    #plt.errorbar(df1.t, df1.U, yerr=df1.dU, fmt='ro', capsize=3, label=label1)
    plt.errorbar(df2.t, df2.U, yerr=df2.dU, fmt='bo', capsize=3, label=label2)
    plt.title('Nissani aku mõõtmised (2024)', fontsize=16)
    plt.xlabel('aeg', fontsize=14)
    plt.ylabel('väljundpinge $U \; / \; \mathrm{V}$', fontsize=14)
    
    esimene_kp = df2.t.iloc[0].floor(freq='24h')
    viimane_kp = df2.t.iloc[-1].ceil(freq='24h')
    paevade_arv = (viimane_kp - esimene_kp).days
    ajatelg = [esimene_kp + pd.Timedelta(dt, 'days') for dt in range(paevade_arv + 1)]
    
    plt.xlim(esimene_kp, viimane_kp)
    plt.xticks(ajatelg, rotation=90)
    plt.ylim(11.9, 13.2)
    plt.yticks(arange(11.9, 13.21, 0.1))
    #plt.legend(loc='best', fontsize=12)
    plt.grid()
    plt.tight_layout()
    plt.savefig('NissaniAkuGraafik.svg')
    plt.show()


def graafik2(df_aku):
    url = 'https://meteo.physic.ut.ee/et/archive.php?do=data&begin%5Byear%5D={}&begin%5Bmon%5D={}&begin%5Bmday%5D={}&end%5Byear%5D={}&end%5Bmon%5D={}&end%5Bmday%5D={}&9=1&12=1&ok=+Esita+p%C3%A4ring+'
    kp1 = df_aku.t.iloc[0].floor(freq='24h')
    kp2 = df_aku.t.iloc[-1].ceil(freq='24h')
    url_met = url.format(kp1.year, kp1.month, kp1.day, kp2.year, kp2.month, kp2.day)
    df_met = pd.read_table(url_met, sep=', ', parse_dates=[0])

    plt.figure(figsize=(10.5, 7))
    plt.subplot(211)
    plt.errorbar(df_aku.t, df_aku.U, yerr=df_aku.dU, fmt='bo', capsize=3)
    plt.title('Nissani aku mõõtmised (2024)', fontsize=16)
    plt.ylabel('väljundpinge $U \; / \; \mathrm{V}$', fontsize=14)
    paevade_arv = (kp2 - kp1).days
    ajatelg = [kp1 + pd.Timedelta(dt, 'days') for dt in range(paevade_arv + 1)]
    plt.xlim(kp1, kp2)
    plt.xticks(ajatelg, rotation=90)
    plt.ylim(11.9, 13.2)
    plt.yticks(arange(11.9, 13.21, 0.1))
    plt.grid()

    plt.subplot(212)
    plt.plot(df_met['Aeg'], df_met['Temperatuur'], 'r-', label=df_met.columns[1])
    plt.title('Võrdlusandmed (meteo.physic.ut.ee)', fontsize=14)
    plt.ylabel('Temperatuur $T \; / \; {}^{\circ} \mathrm{C}$', fontsize=14, c='r')
    plt.grid()
    plt.tick_params('x', labelbottom=False)
    plt.twinx()
    plt.plot(df_met['Aeg'], df_met['Niiskus'], 'b-', label=df_met.columns[2])
    plt.ylabel('Niiskus $\; / \; \%$', fontsize=14, c='b')
    
    plt.xlim(kp1, kp2)
    plt.xticks(ajatelg, rotation=90)
    plt.tick_params('x', labelbottom=False)
    #plt.xlabel('aeg', fontsize=14)
    plt.tight_layout()
    plt.savefig('NissaniAkuGraafik2.svg')
    plt.show()


def juht():
    df1, df2 = mootmistulemused(True)
    graafik1(df1, df2)
    #graafik2(df2)
    
    
if __name__ == '__main__':
    juht()
    #test()
